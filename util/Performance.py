'''
Created on Dec 12, 2014

This module evaluates the performance of a recommendation system
'''
from numpy import linalg, sqrt, array, isnan

def get_RMSE(predictor, test_data):
    ''' 
    @param predictor: A class that implements the predict() function e.g Baseline
    @see: ParseCSV.Input_Data.get_test_data() for the format of test_data
    '''
    
    predicted_ratings = array([predictor.predict(u, i) for (u, i, _) in test_data])
    expected_ratings = array([x for (_, _, x) in test_data])
    num_ratings = len(test_data)
    
    rmse=linalg.norm(predicted_ratings-expected_ratings)/sqrt(num_ratings)   
    return rmse
        

def get_training_RMSE(predictor):
    '''
    Compute the RMSE for the training data used in the predictor.
    '''
    training_data = predictor.training_data
    (num_users, num_fields) = training_data.shape
    test_data = [ (u, f, training_data[u, f]) for u in range(num_users) 
                 for f in range(num_fields) if not isnan(training_data[u, f])]
    
    return get_RMSE(predictor, test_data)


def get_k_fold_RMSE(predictor_creator, k=10):
    from ParseCSV import Input_Data
    input_data = Input_Data('base.csv')
    test_training = input_data.get_k_fold_split(k)
    num = len(test_training)
    RMSE = 0.
    for (test, training) in test_training:
        RMSE += get_RMSE(predictor_creator(training), test)
    return  RMSE / num


def main():
    from Recommender import Recommender
    from Neighborhood import Neighborhood
    from Baseline import Baseline
    
    print "Baseline k-fold: ", get_k_fold_RMSE(Baseline)
    Neighborhood_creator = lambda x: Neighborhood(Baseline(x))
    print "Neighborhood k-fold: ", get_k_fold_RMSE(Neighborhood_creator)
    print "Recommender k-fold: ", get_k_fold_RMSE(Recommender)


if __name__ == "__main__":
    main()