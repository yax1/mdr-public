'''
Module that handles the Baseline Predictor.

Given an m X n matrix, m users and n fields, find: 
    1. The average rating r_bar, (r bar in textbook).
    2. The m X 1 vector of user biases (b_u^* in textbook).
    3. The n X 1 vector of field biases (b_i^* in textbook). 

Input: rating matrix
Output: R_hat matrix 
'''

''' 
r is the average of all the ratings received.
Gotchas: Do not put 0 for movies that do not have ratings
'''


''' 
We calculate the b_u^* and the b_i^* using the average rating, r,
the actual rating of the movie and the system of equations in section 
4.3.1 (p.78). 

We solved this to get a matrix b in Homework 2, Question 4 by minimizing 
the equation at the bottom of p. 71.

b = (A.' * A)\(A.'*c);

b matrix - users first, we solve for this

A matrix dimension - columns (m+n), rows (number of nonzero ratings)
To create:
Loop through each rating in the big rating matrix, each row is 
a rating. 
Fill A[row][i] = 1 and A[row][j] = 1 where the rating
was found at coordinate i, j in the big rating matrix.

c matrix - rows (number of nonzero ratings, same as A), values are rating - r_bar.
For each row in A, there should be exactly two 1's, one in the first m entries 
and one in the last n entries, the rest should be zero.

'''
import numpy as np

class Baseline():
    
    def __init__(self, rating_matrix):
        '''
        Takes in a numpy.matrix which corresponds to the training data
        and creates a predictor.
        '''
        (num_users, num_fields) = rating_matrix.shape
        num_ratings = np.sum(~np.isnan(rating_matrix))
        
        sum_ratings = 0
        A = np.zeros((num_ratings, num_users + num_fields))
        C = np.zeros((num_ratings, 1))
        
        # build matrix A
        index = 0
        for i in range(0, num_users):
            for j in range(0, num_fields):
                if not np.isnan(rating_matrix[i, j]):
                    A[index, i] = 1
                    A[index, num_users + j] = 1
                    index = index + 1
                    sum_ratings = sum_ratings + rating_matrix[i, j]
        
        r_bar = sum_ratings / num_ratings
        # print "r_bar is", r_bar
        
        # build matrix C
        index = 0
        for i in range(0, num_users):
            for j in range(0, num_fields):
                if not np.isnan(rating_matrix[i, j]):
                    C[index, 0] = rating_matrix[i, j] - r_bar
                    index = index + 1
        
        # TODO: what should be set to be lambda?
        lam = 0
        I = np.eye(num_users + num_fields, num_users + num_fields)
        
        b = np.linalg.lstsq(np.dot(A.T, A)- lam * I, np.dot(A.T, C))[0]
        
        #@Yolanda: How is this part checking against the book?
        # check against book answer on p.78, doesn't include normalization
        b_book = np.linalg.lstsq(A, C)[0]
        
        # print "b is: "
        # print b
        # print "b on p.78 is: "
        # print b_book
        
        # calculate R_hat (np.nan represents no rating available.)
        R_hat = np.empty((num_users, num_fields))
        R_hat[:] = np.nan
         
        for i in range(0, num_users):
            for j in range(0, num_fields):
                if not np.isnan(rating_matrix[i, j]):
                    R_hat[i, j] = r_bar + b[i] + b[num_users + j]
        
        self.R = rating_matrix
        self.R_hat = R_hat
        self.r_bar = r_bar
        self.b_users = b[:num_users]
        self.b_fields = b[num_users:]
        self.training_data = self.R
        
        
    def predict(self, user, field):
        '''
        Return a prediction (between 1 and 5) for what user
        will rate the field.
        '''
        prediction = self.r_bar + self.b_users[user] + self.b_fields[field]          
        # Cap prediction
        return  prediction.clip(1., 5.)[0]
    

def sample_baseline():
    '''
    Returns a sample Baseline predictor for debugging purposes.
    '''
    from ParseCSV import sample_training_data
    return Baseline(sample_training_data())


if __name__ == "__main__":
    sample = sample_baseline()
    print sample.R_hat
    from Performance import get_RMSE, get_training_RMSE
    from ParseCSV import sample_test_data
    print "Test RMSE:", get_RMSE(sample, sample_test_data())
    print "Training RMSE", get_training_RMSE(sample)
