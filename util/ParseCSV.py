import csv
import numpy as np
import random

#Index into this array to get the string corresponding to the field category.
#If you change this, edit the docs for accounts.models.new_Rating()
FIELD_CATEGORY = ["Movies", "TV Shows", "Books"]
NUM_CATEGORIES = len(FIELD_CATEGORY)
NUM_DIGITS = len(str(NUM_CATEGORIES))

NAMES_PER_CATEGORY = [50, 50, 50]
START_INDICES = np.cumsum(NAMES_PER_CATEGORY) - NAMES_PER_CATEGORY[0]

# Number of users in the initial trial run.
NUM_OF_BASE_USERS = 12

class Input_Data:
    def __init__(self, filename):
        with open(filename, 'rU') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            x = list(reader)
            R = np.array(x)[1:,:]
            R[R == ''] = np.nan
            R = R.astype('float')
            self.R = R

            self.titles = np.array(x)[0,:]

            (users, _) = np.shape(R)
            
            # This is for debugging. It reduces the randomness of test selection.
#             random.seed(17)

            training_data = np.copy(self.R)
            test_data = []
            field_range_min = 0
            field_range_max = 49
            for u in range(users):
                rand_test = random.randint(field_range_min, field_range_max)
                while np.isnan(R[u, rand_test]):
                    rand_test = random.randint(field_range_min, field_range_max)
                test_data.append((u, rand_test, R[u, rand_test]))
                training_data[u, rand_test] = np.nan

            self.test_data = test_data
            self.training_data = training_data
            

    def get_k_fold_split(self, k):
        '''
        Returns a list of tuples of (test_data, training_data)
        '''
        (users, _) = self.R.shape
        filled_indices = [(~np.isnan(self.R[u,:])).nonzero()[0] for u in range(users)]
        split_indices = [random_partition(indx, k) for indx in filled_indices]
        
        test_indices = [[idx[part] for idx in split_indices] for part in range(k)]
        return [self.get_test_training_pair(idx_list) for idx_list in test_indices]
    
    def get_test_training_pair(self, test_list):
        training_data = np.copy(self.R)
        users = len(test_list)
        test_data = []
        for u in range(users):
            indices = test_list[u]
            testing = [(u, idx, training_data[u, idx]) for idx in indices]
            training_data[u, indices] = np.nan
            test_data.extend(testing)
        return (test_data, training_data)
    
    def get_rating_matrix(self):
        '''
        Get all the rating data both the training and the test data
        '''
        return self.R
    
    def get_test_data(self):
        '''
        Get the data that will be used to test the predictor.
        The data will be a list of tuples e.g. [(user_id, field_id, rating), ...]
        '''
        return self.test_data
    
    def get_training_data(self):
        '''
        Get the data that will be used to train the predictor
        '''
        return self.training_data

    def get_titles(self):
        '''
        Get all the titles of the interests.
        '''
        return self.titles
    
    def get_movie_title(self, index):
        '''
        Get a particular movie title.
        '''
        return self.titles[index + START_INDICES[0]] 
    
    def get_tv_title(self, index):
        '''
        Get a particular tv show title.
        '''
        return self.titles[index + START_INDICES[1]]
    
    def get_book_title(self, index):
        '''
        Get a particular book title.
        '''
        return self.titles[index + START_INDICES[2]] 
    
    def get_movie_index(self, title):
        '''
        Get index for a particular movie title.
        '''
        return np.where(self.titles==title)[0][0]
    
    def get_tv_index(self, title):
        '''
        Get index for a particular tv title.
        '''
        return np.where(self.titles==title)[0][0] - START_INDICES[1]
    
    def get_book_index(self, title):
        '''
        Get index for a particular book title.
        '''
        return np.where(self.titles==title)[0][0] - START_INDICES[2]

    def get_movies_training_data(self):
        '''
        Get movie rating data
        '''
        return self.training_data[:,0:50]

    def get_movies_test_data(self):
        '''
        Get all the movie titles
        '''
        return self.test_data[0:50]
    


def random_partition(lst, n):
    from random import shuffle
    shuffle(lst) 
    division = len(lst) / float(n) 
    return [ lst[int(round(division * i)): int(round(division * (i + 1)))] for i in xrange(n) ]




def sample_rating_matrix(self):
    # test matrix from p. 78, 10 users and 5 movies, 40 ratings
    return np.array([[5., 4., 4., np.nan, 5.],
                            [np.nan, 3., 5., 3., 4.],
                            [5., 2., np.nan, 2., 3.],
                            [np.nan, 2., 3., 1., 2.],
                            [4., np.nan, 5., 4., 5.],
                            [5., 3., np.nan, 3., 5.],
                            [3., 2., 3., 2., np.nan],
                            [5., 3., 4., np.nan, 5.],
                            [4., 2., 5., 4., np.nan],
                            [5., np.nan, 5., 3., 4.]])

def sample_test_data():
    # test data from p. 78, 10 users and 5 movies, 10 ratings
    return [(0, 4, 5), (1, 3, 3), (2, 3, 3), (3, 1, 2),
            (4, 2, 5), (5, 0, 5), (6, 1, 2), (7, 1, 3),
            (8, 0, 4), (9, 0, 5)]

def sample_training_data():
    # test matrix from p. 78, 10 users and 5 movies, 30 ratings
    return np.array([[5., 4., 4., np.nan, np.nan],
                            [np.nan, 3., 5., np.nan, 4.],
                            [5., 2., np.nan, np.nan, 3.],
                            [np.nan, np.nan, 3., 1., 2.],
                            [4., np.nan, np.nan, 4., 5.],
                            [np.nan, 3., np.nan, 3., 5.],
                            [3., np.nan, 3., 2., np.nan],
                            [5., np.nan, 4., np.nan, 5.],
                            [np.nan, 2., 5., 4., np.nan],
                            [np.nan, np.nan, 5., 3., 4.]])

def main():
    input_data = Input_Data('base.csv')
    print input_data.get_k_fold_split(10)
#     print input_data.get_rating_matrix()[0,:]


if __name__ == "__main__":
    main()
