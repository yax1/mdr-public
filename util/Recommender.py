'''
Created on Dec 9, 2014

Main file for the recommendation system.
'''
import random
import numpy as np
from ParseCSV import Input_Data, START_INDICES
from Baseline import Baseline
from Neighborhood import Neighborhood, make_similarity_matrix
from Performance import get_training_RMSE, get_RMSE
from Magic import find_most_similar_users

class Recommender():
    ''' A class for making the recommendation.
    We can add some special sauce for multidimensionality here.
    '''
    def __init__(self, ratings_matrix):
        base_predict = Baseline(ratings_matrix)
        self.neighborhood_predict = Neighborhood(base_predict)
        self.training_data = self.neighborhood_predict.training_data
        self.big_matrix = ratings_matrix
        self.user_movie_similarity = make_similarity_matrix(np.transpose(ratings_matrix[:, START_INDICES[0]:START_INDICES[1]]))
        self.user_tv_similarity = make_similarity_matrix(np.transpose(ratings_matrix[:, START_INDICES[1]:START_INDICES[2]]))
        self.user_book_similarity = make_similarity_matrix(np.transpose(ratings_matrix[:, START_INDICES[2]:]))

        
    def predict(self, user_row_num, index):
        similar1 = None
        similar2 = None
        
        if index < START_INDICES[1]: # is movie
            similar1 = self.user_tv_similarity
            similar2 = self.user_book_similarity
        elif index < START_INDICES[2]: # is show
            similar1 = self.user_movie_similarity
            similar2 = self.user_book_similarity
        else: # is book
            similar1 = self.user_movie_similarity
            similar2 = self.user_tv_similarity
        
        similarUsers = find_most_similar_users(similar1, user_row_num)
        similarUsers.extend(find_most_similar_users(similar2, user_row_num))

        magicPrediction = 0
        numSimilar = 0.  # Adding decimal point just to ensure no integer division.
        for i in similarUsers:
            if not np.isnan(self.big_matrix[i, index]):
                magicPrediction = magicPrediction + self.big_matrix[i, index]
                numSimilar = numSimilar + 1
        
        if numSimilar > 0.:
            magicPrediction = magicPrediction / numSimilar

        neighborhoodPrediction = self.neighborhood_predict.predict(user_row_num, index)

        magicWeight = 0.1
        averagePrediction = magicWeight * magicPrediction + (1.0 - magicWeight) * neighborhoodPrediction

        # add Randomness
        randomnessCoefficient = 0.01# 0.05

        #return neighborhoodPrediction
        return  (1 - randomnessCoefficient) * averagePrediction + randomnessCoefficient * random.uniform(-5, 5)
        
def main():
    input_data = Input_Data('base.csv')
    recommend_with_MDR = Recommender(input_data.get_training_data())
    base_predict = Baseline(input_data.get_training_data())
    recommend_without_MDR = Neighborhood(base_predict)
    
    print "Recommend with MDR"
    print "Test RMSE:", get_RMSE(recommend_with_MDR, input_data.get_test_data()) 
    print "Training RMSE", get_training_RMSE(recommend_with_MDR)

    print "Recommend without MDR"
    print "Test RMSE:", get_RMSE(recommend_without_MDR, input_data.get_test_data()) 
    print "Training RMSE", get_training_RMSE(recommend_without_MDR)

    
    '''
    base_predict = Baseline(input_data.get_training_data())
    neighborhood_predict = Neighborhood(base_predict)
    print "Baseline Test RMSE:", get_RMSE(base_predict, input_data.get_test_data()) 
    #print "Baseline Training RMSE", get_training_RMSE(base_predict)

    print "Neighborhood Test RMSE:", get_RMSE(neighborhood_predict, input_data.get_test_data())
    #print "Neighborhood Training RMSE", get_training_RMSE(neighborhood_predict)

    movies_base_predict = Baseline(input_data.get_movies_training_data())
    movies_neighborhood_predict = Neighborhood(movies_base_predict)

    '''
    '''
    print "Movies Only Baseline Test RMSE:", get_RMSE(movies_base_predict, input_data.get_movies_test_data())
    print "Movies Only Baseline Training RMSE", get_training_RMSE(movies_base_predict)

    print "Movies Only Neighborhood Test RMSE:", get_RMSE(movies_neighborhood_predict, input_data.get_movies_test_data())
    print "Movies Only Neighborhood Training RMSE", get_training_RMSE(movies_neighborhood_predict)
    '''

if __name__ == "__main__":
    main()
