'''
Module that makes use of the multidimensionality of the interests

'''
import numpy as np

def find_most_similar_users(R, userRowNumber):
    '''
    Given a similarity matrix and a userRowNumber, return the row numbers
    of the most similar users
    '''
    num_similar_users = 2
    similarUsers = np.zeros(num_similar_users)
    userRow = R[userRowNumber]
    similarUserRows = sorted(range(len(userRow)), key=lambda i:userRow[i])[-num_similar_users:]
    
    return similarUserRows



if __name__ == "__main__":
    from Neighborhood import cosine_similarity, make_similarity_matrix


    m = np.array([[5., 4., 4., np.nan, np.nan],
                [5., 4., 4., np.nan, np.nan],
                [5., 4., 3., np.nan, np.nan],
                [1., 2., 2., np.nan, np.nan]])

    print find_most_similar_users(make_similarity_matrix(m), 0)