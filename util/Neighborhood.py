'''
Module that handles the neighborhood model

'''
import numpy as np
from math import sqrt

def cosine_similarity(v1, v2):
        d1 = d2 = 0.0
        numerator = 0.0
        for i in range(len(v1)):
            if np.isnan(v1[i]) or np.isnan(v2[i]):
                pass
            else:
                numerator = numerator + v1[i] * v2[i]
                d1 = d1 + v1[i] * v1[i]
                d2 = d2 + v2[i] * v2[i]
        denominator = sqrt(d1 * d2)
    
        if denominator:
            return numerator/denominator
        else:
            return 0.0

def make_similarity_matrix(R):
        '''
        This returns a num_fields X num_fields similarity matrix
        between two types of fields.
        '''
        (_, fields) = np.shape(R)
        D = np.empty((fields, fields))
        D[:] = np.nan
        for i in range(fields):
            for j in range(i + 1, fields):
                D[i][j] = cosine_similarity(R[:,i], R[:,j])
                D[j][i] = D[i][j]
        return D


class Neighborhood:
    def __init__(self, baseline):
        '''
        The neighborhood method builds on the baseline method.
        So we pass the baseline as an input.
        '''
        self.training_data = baseline.training_data
        self.baseline = baseline
        self.R_tilde = baseline.R - baseline.R_hat
        self.similarity_matrix = make_similarity_matrix(self.R_tilde)
        
    def predict(self, user, field, num_neighbors=2):
        '''
        Return a prediction (between 1 and 5) for what user
        will rate the field.
        @param num_neighbors: The number of neighbors of a field to consider.
        '''
        
        #1 Find the nearest num_neighbors neighbors of field
        weights = np.abs(self.similarity_matrix[field])
        
        nearest_fields = [0]*num_neighbors
        temp = np.copy(weights)
        for i in range(num_neighbors):
            index = np.nanargmax(temp)
            temp[index] = np.nan
            nearest_fields[i] = index
        
        # Works beautifully but is only available from numpy 1.8.0
#         last_index = -num_neighbors -1
#         weights.argpartition(last_index)[last_index:]
#         nearest_fields = [x for x in nearest_fields if x != field]
        
        
        #2 Find the user's r_tilde for the neighbors if any.
        r_tilde = self.R_tilde[user]
        similarity = self.similarity_matrix[field]
        neighborhood = [(weights[i], similarity[i], r_tilde[i]) 
           for i in nearest_fields if not np.isnan(r_tilde[i])]
        
        #3 Find prediction
            # If user didn't rate any, use baseline.
            # Else use formula on p.80 
        prediction = self.baseline.r_bar + self.baseline.b_users[user] 
        prediction += self.baseline.b_fields[field]   
        num = sum([similarity*r_tilde  for (_, similarity, r_tilde) in neighborhood])
        denom = sum([weight for (weight, _,_) in neighborhood])                     
        
        if denom:
            prediction += num/denom 
        # Cap prediction
        return prediction.clip(1,5)[0]

    
def sample_neighborhood():
    '''
    Returns a sample Neighborhood predictor for debugging purposes.
    '''
    from Baseline import sample_baseline
    return Neighborhood(sample_baseline())
    

if __name__ == "__main__":
    from Performance import get_RMSE, get_training_RMSE
    from ParseCSV import sample_test_data
    sample = sample_neighborhood()
    print "Test RMSE:", get_RMSE(sample, sample_test_data())
    print "Training RMSE:", get_training_RMSE(sample)
    print sample.similarity_matrix

