from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'webapp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    url(r'^$', 'home.views.home', name='home'),
    url(r'^submit$', 'home.views.submit', name='submit'),
    url(r'^predict$', 'home.views.predict', name='predict'),
    url(r'^accounts$', 'accounts.views.home', name='account'),
    url(r'^admin/', include(admin.site.urls)),
)
