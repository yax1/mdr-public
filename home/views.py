from django.shortcuts import render
from django.http import HttpResponse
from google.appengine.api import users
from accounts.models import get_UserAccount, new_Rating
from util.ParseCSV import Input_Data
import json
import random
import numpy as np

def home(request):
    input_data = Input_Data('util/base.csv')
       
    result = getInitialTitlesIds(input_data)
    movie_titles = result["movie_titles"]
    movie_title_ids = result["movie_title_ids"]
    
    movie_titles["ids"] = movie_title_ids
    result["movie_titles"] = movie_titles
    
    tv_titles = result["tv_titles"]
    tv_title_ids = result["tv_title_ids"]
    
    tv_titles["ids"] = tv_title_ids
    result["tv_titles"] = tv_titles
    
    book_titles = result["book_titles"]
    book_title_ids = result["book_title_ids"]
    
    book_titles["ids"] = book_title_ids
    result["book_titles"] = book_titles
    
    if request.method == 'POST' and request.is_ajax():
        data = json.dumps({})
        genre = request.POST.get('genre')

        ids = request.POST.get('ids')
        newId = getNewId(ids)
        
        if genre == "movies":
            newTitle = input_data.get_movie_title(newId)
        elif genre == "shows":
            newTitle = input_data.get_tv_title(newId)
        elif genre == "books":
            newTitle = input_data.get_book_title(newId)
        else:
            pass
        ids = appendId(ids, newId) 
        
        data = json.dumps({'title': newTitle})        
        return HttpResponse(data)
    else:
        return render(request, 'index.html', result)

def getInitialTitlesIds(input_data):
    result = {}
    movie_titles = {}
    movie_title_ids = ""
    tv_titles = {}
    tv_title_ids = ""
    book_titles = {}
    book_title_ids = ""
    
    newId = getNewId(book_title_ids)
    movie_titles["one"] = input_data.get_movie_title(newId)
    movie_title_ids = appendId(movie_title_ids, newId)
    tv_titles["one"] = input_data.get_tv_title(newId)
    tv_title_ids = appendId(tv_title_ids, newId)
    book_titles["one"] = input_data.get_book_title(newId)
    book_title_ids = appendId(book_title_ids, newId)  
    
    newId = getNewId(book_title_ids)
    movie_titles["two"] = input_data.get_movie_title(newId)
    movie_title_ids = appendId(movie_title_ids, newId)
    tv_titles["two"] = input_data.get_tv_title(newId)
    tv_title_ids = appendId(tv_title_ids, newId)
    book_titles["two"] = input_data.get_book_title(newId)
    book_title_ids = appendId(book_title_ids, newId) 
    
    newId = getNewId(book_title_ids)
    movie_titles["three"] = input_data.get_movie_title(newId)
    movie_title_ids = appendId(movie_title_ids, newId)
    tv_titles["three"] = input_data.get_tv_title(newId)
    tv_title_ids = appendId(tv_title_ids, newId)
    book_titles["three"] = input_data.get_book_title(newId)
    book_title_ids = appendId(book_title_ids, newId) 
    
    newId = getNewId(book_title_ids)
    movie_titles["four"] = input_data.get_movie_title(newId)
    movie_title_ids = appendId(movie_title_ids, newId)
    tv_titles["four"] = input_data.get_tv_title(newId)
    tv_title_ids = appendId(tv_title_ids, newId)
    book_titles["four"] = input_data.get_book_title(newId)
    book_title_ids = appendId(book_title_ids, newId) 
      
    newId = getNewId(book_title_ids)
    movie_titles["five"] = input_data.get_movie_title(newId)
    movie_title_ids = appendId(movie_title_ids, newId)
    tv_titles["five"] = input_data.get_tv_title(newId)
    tv_title_ids = appendId(tv_title_ids, newId)
    book_titles["five"] = input_data.get_book_title(newId)
    book_title_ids = appendId(book_title_ids, newId) 
    
    result["movie_titles"] = movie_titles
    result["movie_title_ids"] = movie_title_ids
    result["tv_titles"] = tv_titles
    result["tv_title_ids"] = tv_title_ids
    result["book_titles"] = book_titles
    result["book_title_ids"] = book_title_ids
    
    return result
    
def getNewId(book_title_ids):
    ids_array = book_title_ids.split(",")
    field_range_min = 0
    field_range_max = 49
    newId = str(random.randint(field_range_min, field_range_max))
    while newId in ids_array:
        newId = str(random.randint(field_range_min, field_range_max))
    return int(newId)

def appendId(ids, newId):
    return "" + ids + "," + str(newId)

def submit(request):
    user = users.get_current_user()
    user_account = get_UserAccount(user)

    if request.method == 'POST':
        input_data = Input_Data('util/base.csv')
        titles = request.POST.get('titles')
        ratings = request.POST.get('ratings')
        genres = request.POST.get('genres')
        titles = json.loads(titles)
        ratings = json.loads(ratings)
        genres = json.loads(genres)
        
        all_rates = []
        for i in range(len(ratings)):
            if not ratings[i] == 0:             
                if genres[i] == "movies":
                    id = input_data.get_movie_index(titles[i])
                    rate = new_Rating(id, 0 , int(ratings[i]))
                elif genres[i] == "shows":
                    id = input_data.get_tv_index(titles[i])
                    rate = new_Rating(id, 1 , int(ratings[i]))
                elif genres[i] == "books":
                    id = input_data.get_book_index(titles[i])
                    rate = new_Rating(id, 2 , int(ratings[i]))
                else:
                    pass
                all_rates.append(rate)
        user_account.add_ratings(all_rates)
        
        #count = np.count_nonzero(~np.isnan(ratings_row))
        
    #data = json.dumps({'count': count})   
    return HttpResponse("")
    

def predict(request):
    from util.ParseCSV import NAMES_PER_CATEGORY, START_INDICES
    result = {}
    movie_recs_title = {}
    tv_recs_title = {}
    book_recs_title = {}
    user = users.get_current_user()
    user_account = get_UserAccount(user)
    input_data = Input_Data('util/base.csv')
    
    num_recs = 2
    
#     print "ratings in predict ", user_account.get_ratings_row()
    predictions = user_account.get_all_predictions()
    movie_pred = predictions[START_INDICES[0]:START_INDICES[0] + NAMES_PER_CATEGORY[0]]
    tv_pred = predictions[START_INDICES[1]:START_INDICES[1] + NAMES_PER_CATEGORY[1]]
    book_pred = predictions[START_INDICES[2]:START_INDICES[2] + NAMES_PER_CATEGORY[2]]
    
    movie_recs = sorted(range(len(movie_pred)), key=lambda i:movie_pred[i])[-num_recs:]      
    tv_recs = sorted(range(len(tv_pred)), key=lambda i:tv_pred[i])[-num_recs:] 
    book_recs = sorted(range(len(book_pred)), key=lambda i:book_pred[i])[-num_recs:] 
    
    movie_recs_title["one"] = input_data.get_movie_title(movie_recs[0])
    movie_recs_title["two"] = input_data.get_movie_title(movie_recs[1])
    tv_recs_title["one"] = input_data.get_tv_title(tv_recs[0])
    tv_recs_title["two"] = input_data.get_tv_title(tv_recs[1])
    book_recs_title["one"] = input_data.get_book_title(book_recs[0])
    book_recs_title["two"] = input_data.get_book_title(book_recs[1])
    
    result["movie_recs"] = movie_recs_title
    result["tv_recs"] = tv_recs_title
    result["book_recs"] = book_recs_title
    
    return render(request, 'results.html', result)

