from google.appengine.ext import ndb
from util.ParseCSV import FIELD_CATEGORY, NUM_CATEGORIES, NUM_DIGITS, NAMES_PER_CATEGORY, START_INDICES

class Rating(ndb.Model):
    ''' Use new_Rating to create a new Rating. 
    It handles error checking.
    '''
    field_id=ndb.IntegerProperty("n")
    field_category=ndb.IntegerProperty("c")
    value=ndb.IntegerProperty("v")
    
    def __unicode__(self):
        strs = "Id: " + str(self.field_id) + ", Category: " + str(self.field_category)
        return strs + ", Rating: " + str(self.value)

    
    def __hash__(self):
        ''' Provides a hash such that objects with the same field_id 
        and field_category have the same hash.
        '''
        id_hash = self.field_id * 10 ** NUM_DIGITS
        return id_hash + self.field_category
     
    def __eq__(self, other):
        '''
        Compares such that objects with the same field_id 
        and field_category are equal.
        '''
        match = self.field_category == other.field_category
        return match and (self.field_id == other.field_id)
    
    
def new_Rating(id, category, value):
        ''' Creates a new rating. 
        Category is a number such that 
        0 - "Movies", 1 - "TV Shows", 2 - "Books"
        '''
        #Check validity of category
        try:
            FIELD_CATEGORY[category]
        except:
            msg = "Category should be within 0 and %i" % (NUM_CATEGORIES - 1)
            raise Exception(msg)
            
        return Rating(field_id = id, field_category = category, value = value)
    

class UserAccount(ndb.Model):
    ratings = ndb.StructuredProperty(Rating, repeated=True)
    user = ndb.UserProperty()
    user_row_num = ndb.IntegerProperty("rn")
    
    def __unicode__(self):
        return self.user.nickname()
    
    def add_ratings(self, ratings):
        ''' Add all the ratings to the pre-existing list of ratings for the user.
        Overwrite old values if already exists.
        '''        
        ratings.extend(self.ratings)
        overwrite = set(ratings)
        self.ratings = list(overwrite)
        self.put()
        
        
    def get_ratings_row(self):
        from numpy import empty, nan
        num_fields = sum(NAMES_PER_CATEGORY)
        new_row = empty(num_fields)
        new_row[:] = nan
        
        # Fill in the ratings_row
        for rating in self.ratings:
            index = START_INDICES[rating.field_category] + rating.field_id
            new_row[index] = rating.value
        
        return new_row
    
    def get_all_predictions(self):
        from numpy import empty, isnan
        
        num_fields = sum(NAMES_PER_CATEGORY)
        predictions = empty(num_fields)
        predictions[:] = 0
        
        recommend = get_Recommender() 
        
        for i in range(NUM_CATEGORIES):
            for j in range(NAMES_PER_CATEGORY[i]):
                index = START_INDICES[i] + j
                predictions[index] = self.get_quick_prediction(recommend, j, i)

        return predictions
    
    def get_quick_prediction(self, recommend, field_id, field_category):
        index = START_INDICES[field_category] + field_id
        return recommend.predict(self.user_row_num, index)  
    
    def get_prediction(self, field_id, field_category):
        recommend = get_Recommender() 
        index = START_INDICES[field_category] + field_id
        return recommend.predict(self.user_row_num, index)

    
def get_UserAccount(user):
    '''
    Returns the UserAccount associated with the user.
    Creates a new UserAccount only if the user has none.
    '''    
    currentAccount = UserAccount.query(UserAccount.user == user).get()
    if currentAccount:
        return currentAccount
    else:
        from util.ParseCSV import NUM_OF_BASE_USERS
        user_row_num = UserAccount.query().count() + NUM_OF_BASE_USERS
        new_user = UserAccount(user = user, user_row_num=user_row_num)
        new_user.put()
        return new_user


def get_Recommender():
        from util.ParseCSV import Input_Data, NUM_OF_BASE_USERS
        from numpy import empty
        
        all_users = UserAccount.query()
        num_users = all_users.count() + NUM_OF_BASE_USERS
        num_fields = sum(NAMES_PER_CATEGORY)
        new_matrix = empty((num_users, num_fields))
        
        input_data = Input_Data('util/base.csv')
        new_matrix[:NUM_OF_BASE_USERS, :] = input_data.get_rating_matrix()

        # Populate the rest of the matrix
        for num in range(NUM_OF_BASE_USERS, num_users):
            user = all_users.filter(UserAccount.user_row_num == num).get()
            new_matrix[num, :] = user.get_ratings_row()
        
        from util.Recommender import Recommender
        return Recommender(new_matrix)
        
    
if __name__ == '__main__':
    pass

    
