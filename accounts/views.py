from django.http import HttpResponse
from google.appengine.api import users
from models import new_Rating, get_UserAccount

# Create your views here.
def home(request):
    ''' Example of how to manipulate the views'''
    user = users.get_current_user()
    message = ""
    if not user:
        message = '<a href="%s">Sign in or register</a>.' % users.create_login_url('/')
    else:
        message = 'Hello, %s! <a href="%s">Logout</a>' % (user.nickname(), users.create_logout_url('/'))
    rate = new_Rating(43, 0 , 1)
    rate2 = new_Rating(27, 1 , 1)
    replace = new_Rating(43, 0 , 123)
    listR = [rate, rate2]
    me = get_UserAccount(user)
 #   print me.get_ratings_row()
    me.add_ratings(listR)
#     me.ratings = []
#     me.add_ratings([replace])
#     print me.get_ratings_row()
    return HttpResponse(message)