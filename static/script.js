function switchContent() {
	debugger;
	alert('hello!!');
	current.text("Found You!");
}

window.onload = function() {
	var movies = [document.getElementById("movie_title_one").value, 
		document.getElementById("movie_title_two").value, 
		document.getElementById("movie_title_three").value,
		document.getElementById("movie_title_four").value,
		document.getElementById("movie_title_five").value];
	var shows = [document.getElementById("tv_title_one").value, 
		document.getElementById("tv_title_two").value, 
		document.getElementById("tv_title_three").value,
		document.getElementById("tv_title_four").value,
		document.getElementById("tv_title_five").value];
	var books = [document.getElementById("book_title_one").value, 
		document.getElementById("book_title_two").value, 
		document.getElementById("book_title_three").value,
		document.getElementById("book_title_four").value,
		document.getElementById("book_title_five").value];

	var numberOfEachGenre = 5;
	
	

	var boxHTML_part1 = '<div class="box" id="';
	var boxHTML_part2 = '"><p id="';
	var boxHTML_part3 = '"></p>' +
	'<div class="starRating"><div><div><div><div>' +
	'<input id="rating1" type="radio" name="'; 
	var boxHTML_part4 = '" value="1">' +
	'<label for="rating1"><span>1</span></label></div>' +
	'<input id="rating2" type="radio" name="';
	var boxHTML_part5 = '" value="2">' +
	'<label for="rating2"><span>2</span></label></div>' +
	'<input id="rating3" type="radio" name="';
	var boxHTML_part6 = '" value="3">' +
	'<label for="rating3"><span>3</span></label></div>' +
	'<input id="rating4" type="radio" name="';
	var boxHTML_part7 = '" value="4">' +
	'<label for="rating4"><span>4</span></label></div>' +
	'<input id="rating5" type="radio" name="'; 
	var boxHTML_part8 = '" value="5">' +
	'<label for="rating5"><span>5</span></label></div>' +
	'<button type="button" id="delete"><span>Delete</span></button>' + 
	'</div><br>';
	
	

	
	var submitHTMLstring = '<div class="button-align"><button type="button" class="pure-button pure-button-primary " id="submit_ratings" >' +
	'<span>Submit</span></button></div>' + '<form id="submit_ratings_form" action="predict" method="get"></form>';
	$("#submit-button").append(submitHTMLstring);
	
	$("#submit_ratings").click(function() {
		var boxes = $(".box");
		submitRatings(boxes);

	});
	
	for (i = 0; i < numberOfEachGenre; i++) {
		var ratingId = "movieRating_" + i;
		var movieHTMLstring = boxHTML_part1 + "movie_" + i + boxHTML_part2 + "movie_title_" + i +
			boxHTML_part3 + ratingId + boxHTML_part4 + ratingId + boxHTML_part5 + ratingId + 
			boxHTML_part6 + ratingId + boxHTML_part7 + ratingId + boxHTML_part8;
		ratingId = "showRating_" + i;
		var showHTMLstring = boxHTML_part1 + "show_" + i + boxHTML_part2 + "show_title_" + i + 
			boxHTML_part3 + ratingId + boxHTML_part4 + ratingId + boxHTML_part5 + ratingId + 
			boxHTML_part6 + ratingId + boxHTML_part7 + ratingId + boxHTML_part8;
		ratingId = "bookRating_" + i;
		var bookHTMLstring = boxHTML_part1 + "book_" + i + boxHTML_part2 + "book_title_" + i +
			boxHTML_part3 + ratingId + boxHTML_part4 + ratingId + boxHTML_part5 + ratingId + 
			boxHTML_part6 + ratingId + boxHTML_part7 + ratingId + boxHTML_part8;

		$(".column-left").append(movieHTMLstring);
		$(".column-center").append(showHTMLstring);
		$(".column-right").append(bookHTMLstring);

		// box content title id's
		var movieTitle = "movie_title_" + i;
		var showTitle = "show_title_" + i;
		var bookTitle = "book_title_" + i;

		// initialize content titles
		$("#" + movieTitle).text(movies[i]);
		$("#" + showTitle).text(shows[i]); // TODO: replace with shows and books
		$("#" + bookTitle).text(books[i]);

		// box id's
		var movie = "movie_" + i;
		var show = "show_" + i;
		var book = "book_" + i;

		// create delete button event listeners
		$("#" + movie).find("button").click(function() {
			var titleObject = $(this);
			$('#form_change_title').trigger("submit", {titleObject: titleObject, idsObject: $("#movie_title_ids")});
		});
		
		$("#" + show).find("button").click(function() {
			var titleObject = $(this);
			$('#form_change_title').trigger("submit", {titleObject: titleObject, idsObject: $("#tv_title_ids")});
		});
		$("#" + book).find("button").click(function() {
			var titleObject = $(this);
			$('#form_change_title').trigger("submit", {titleObject: titleObject, idsObject: $("#book_title_ids")});
		});

		// create rating click event listeners
		$("#" + movie).find(".starRating").click(function() {
			var rating = getRating($(this));
			var title = $(this).parent().find("p").text();
		});
		$("#" + show).find(".starRating").click(function() {
			var rating = getRating($(this));
			var title = $(this).parent().find("p").text();
		});
		$("#" + book).find(".starRating").click(function() {
			var rating = getRating($(this));
			var title = $(this).parent().find("p").text();
		});
	}

	$('#form_change_title').on('submit', function(event, param){
		event.preventDefault();
		replaceContent(param.titleObject, param.idsObject);
	});
		
}

function submitRatings(boxes) {
	var num = boxes.length;
	var titles = new Array(num);
	var ratings = new Array(num);
	var genres = new Array(num);
	for (i = 0; i < num; i++) {
		ratings[i] = getRating($("#" + boxes[i].id).find(".starRating"));
		titles[i] = $("#" + boxes[i].id).find(".starRating").parent().find("p").text();
		genres[i] = $("#" + boxes[i].id).find(".starRating").parent().parent().attr('id');
	}
	$.post("submit", { titles: JSON.stringify(titles), ratings: JSON.stringify(ratings),  genres: JSON.stringify(genres) }, 
	function(data) {
		$("#submit_ratings_form").submit();
		/*
    	var d = $.parseJSON(data);
    	if (d.count < 0) {
    		alert("Please rate at least 15 titles.");
    	} else {
			$("#submit_ratings_form").submit();
    	}*/
    });
	/*$.ajax({
        url : "submit", 
        type : "POST",
        data : { titles: JSON.stringify(titles), ratings: JSON.stringify(ratings),  genres: JSON.stringify(genres) }, 
        success : function(data) {
        	var d = $.parseJSON(data);
        	if (d.count < 0) {
        		alert("Please rate at least 15 titles.");
        	} else {
				$("#submit_ratings_form").submit();
        	}
        },
    });*/
}

function getRating(ratingBox) {
	var inputs = ratingBox.find("input");
	var rating;
	for (var i=0, len=inputs.length; i<len; i++) {
		if (inputs[i].checked) {
			rating = inputs[i].value;
		}
	}
	if (rating == null) {
		rating = 0;
	}
	return rating;
}

// TODO: either disable X or clear star rating
function replaceContent(titleObject, idsObject) {
	var genre = titleObject.parent().parent().attr('id'); // genre will be "movies", "shows", "books"
 	var title = titleObject.parent().find("p");
 	
 	// clear rating
 	var inputs = titleObject.parent().find("input");
	for (var i=0, len=inputs.length; i<len; i++) {
		if (inputs[i].checked) {
			inputs[i].checked = false;
		}
	}
	$.ajax({
        url : "", 
        type : "POST",
        data : { title: title.text(), ids: idsObject.val(), genre: genre }, 
        success : function(data) {
            var d = $.parseJSON(data);
            title.text(d.title);
            idsObject.val(d.ids);
        },
    });
}


// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

